# Fantom Programming Language

Fantom is an OO, functional programming language designed to cross compile to
Java, .NET, and JavaScript. It provides a concise syntax along with elegant,
cross portable libraries.

Documentation, discussion forums, and issue tracking are located at the
official site:

## **[http://fantom.org/](http://fantom.org/)**