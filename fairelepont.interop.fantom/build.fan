#! /usr/bin/env fan

using build

class Build : BuildPod {

    new make() {
        podName = "fairelepont"
        summary = "Something fresh and clean ..."
        version = Version("0.0.1")

        meta = [
            "org.name"        : "net.sf.hajuergens",
            "org.uri"         : "https://hjuergens.github.io/",
            "proj.name"       : "Faire le pont",
            "proj.uri"        : "https://hjuergens.github.io/fairelepont",
            "vcs.uri"         : "https://httpsbit64@bitbucket.org/httpsbit64/fairelepont.git",
            "license.name"    : "Apache Licence 2.0",
            "repo.private"    : "true",
        ]

        depends = [
            "sys 1.0",
        ]

        srcDirs = [`fan/`,`fan/example/`,`test/`]
        resDirs = [`doc/`]
        resDirs = [`doc/`, `res/`]

        javaDirs = [`java/`]

        docApi = true
        docSrc = true
    }
}
