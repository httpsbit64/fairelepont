package example.msgpack;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import example.avro.User;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.junit.jupiter.api.Test;
import org.apache.avro.file.SeekableByteArrayInput;

import java.io.IOException;
import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {
    @Test
    void viaByteBuffer() throws IOException {
        final User userFrom = new User();
        userFrom.setName("Test");
        userFrom.setFavoriteNumber(7);
        userFrom.setFavoriteColor("blue");

        final ByteBuffer byteBuffer = userFrom.toByteBuffer();
        final User userTo = User.fromByteBuffer(byteBuffer);

        assertEquals(userFrom, userTo);
    }

    @Test
    void viaByteOutputStream() {
        User userIn = User.newBuilder().setName("Speedy")
                .setFavoriteNumber(7).setFavoriteColor("2013").build();

        DatumWriter<User> datumWriter =
                new SpecificDatumWriter<>(User.class);
        DataFileWriter<User> fileWriter =
                new DataFileWriter<>(datumWriter);

        ByteOutputStream bos = new ByteOutputStream();
        try {
            fileWriter.create(userIn.getSchema(), bos);
            fileWriter.append(userIn);
            fileWriter.close();
        } catch (IOException e) {
        }

        SeekableByteArrayInput seekabelInput = new SeekableByteArrayInput(
                bos.getBytes()
        );

        DatumReader<User> datumReader =
                new SpecificDatumReader<>(User.class);
        try {
            DataFileReader<User> fileReader =
                    new DataFileReader<>(seekabelInput, datumReader);

            User userOut = null;

            if (fileReader.hasNext()) {
                userOut = fileReader.next(userOut);
            }

            assertEquals("Speedy", userOut.getName().toString());
            assertEquals(7, userOut.getFavoriteNumber().intValue());
            assertEquals("2013", userOut.getFavoriteColor().toString());


        } catch (IOException e) {
        }
    }
}