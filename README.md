
thrift
------

Install:

[How to install Apache Thrift on Ubuntu 14.04 Trusty](http://www.saltycrane.com/blog/2011/06/install-thrift-ubuntu-1010-maverick/ "2014-10-06: Here's what he did to install on Ubuntu 14.04 Trusty.")

    $ # Install your choice of java
    $ sudo apt-get install libboost-dev libboost-test-dev libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libevent-dev automake libtool flex bison pkg-config g++ libssl-dev 
    $ cd /tmp 
    $ curl http://archive.apache.org/dist/thrift/0.9.1/thrift-0.9.1.tar.gz | tar zx 
    $ cd thrift-0.9.1/ 
    $ ./configure 
    $ make 
    $ sudo make install 
    $ thrift --help 

[Thrift compiler for Windows (thrift-0.9.3.exe)](http://www.apache.org/dyn/closer.cgi?path=/thrift/0.9.3/thrift-0.9.3.exe)
[[PGB]](https://www.apache.org/dist/thrift/0.9.3/thrift-0.9.3.exe.asc)
[[MD5]](https://www.apache.org/dist/thrift/0.9.3/thrift-0.9.3.exe.md5)


protocol-buffers
----------------
[[protocol-buffers]](https://developers.google.com/protocol-buffers/)


-----------------------------------
Icons made by [[Freepik]](http://www.freepik.com) from [[Flaticon]](http://www.flaticon.com)
