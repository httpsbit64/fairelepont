package net.sf.hajuergens;

import org.junit.Assert;
import org.junit.Test;
import net.sf.hajuergens.fairelepont.interop.AddressBookProtos.Person;
import net.sf.hajuergens.fairelepont.interop.AddressBookProtos.Person.PhoneNumber;

public class ServiceHandlerTest {
	@Test
	public void shouldInvokePing() {
        Person john =
				Person.newBuilder()
						.setId(1234)
						.setName("John Doe")
						.setEmail("jdoe@example.com")
						.addPhone(
								PhoneNumber.newBuilder()
										.setNumber("555-4321")
										.setType(Person.PhoneType.HOME).build()
						)
						.build();
		Assert.assertEquals("555-4321", john.getPhone(0).getNumber());
	}

}
