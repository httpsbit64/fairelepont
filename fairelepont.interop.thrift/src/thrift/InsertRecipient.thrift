namespace java net.sf.hajuergens.fairelepont
namespace csharp net.sf.hajuergens.fairelepont

const string VERSION = "0.1"

// https://en.wikipedia.org/wiki/ASCII
enum MessageType
{
    UNDEFINED = 0,
    REQ = 82, // 'R',
    REQACK = 65, //'A',
    IMGREQ = 73, //'I',
    UNREQ = 67, //  'C',
    UPDATE = 85, //  'U',
    MSG_ERROR = 69, //  'E',
    HEARTBEAT = 72, //  'H',
    SREQ = 83, //  'S',
    SNAP = 80, //  'P',
    STATUS = 87, //  'W',
    MATURITY = 77, //  'M',
    TEST = 84, //  'T',
    STALE = 88 , // 'X'
}

typedef string FieldKeyType
typedef string FieldValueType

struct Field
{
    1: required FieldKeyType Key,
    2: required FieldValueType Value,
}

exception Xception {
  1: i32 errorCode,
  2: string message,
}

struct Message
{
    1: required MessageType MessageType,
    2: required string SubscriptionId,
    3: required list<Field> GetFields,
    4: required map<FieldKeyType,FieldValueType> GetFieldValue,
}

typedef i64 CallerType

/// <summary>
/// This is the service interface. The recipient of insert commands.
/// </summary>
/// <remarks>
/// Review the name of this interface!
/// </remarks>
service InsertRecipient
{
    void insert(1:CallerType object, 2:string topicExpression, 3:Message updateMessage) throws(1:Xception exp),
    oneway void start(),
    oneway void stop(),
}

